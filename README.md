# intel-cat_experiments

## Usage

Simply execute the `run` script.

## Dependencies

To install the required dependencies, only Guix with support for
[Inferiors](https://www.gnu.org/software/guix/manual/en/html_node/Inferiors.html).
is needed.
The provided [environment file](./environment.scm) will be loaded by
`run` and will download all needed dependencies.

### Guix-managed dependencies

Please refer to the [deployment README](./deployment/README.md) file for a list of
runtime dependencies for the deployment.

## Repository dependencies

Multiple repositories are used as 
[git-subrepos](https://github.com/ingydotnet/git-subrepo):

- [deployment](git@gitlab.inria.fr:aljanon-experiment-framework/deployment.git)
- [ansible](git@gitlab.inria.fr:aljanon-experiment-framework/ansible_experiment.git)
- [setup](git@gitlab.inria.fr:aljanon-experiment-framework/setup_experiment.git)
- [teardown](git@gitlab.inria.fr:aljanon-experiment-framework/teardown_experiment.git)
