# teardown_experiment

This repository contains a simple Python program used to clean, or collect
additional information after an experiment.

## Collected information

At the moment, `teardown_experiment` only collects `dmesg` information.

## Usage

`teardown_experiment` has one mandatory argument: the path to a writable
directory, where it will create its own `teardown` directory to store
the information it collects.

If you plan to use the provided Pipfile for managing python dependencies, do not
forget to `lock` the file first.

If you have Guix (with support for Inferiors) installed, simply execute the
`run` script. It will read the list of packages and custom repositories from the
[environment.scm](./environment.scm) file. Otherwise, you can install the
packages listed in [Guix-managed dependencies].

## Dependencies

### Python dependencies

The Python dependencies are managed by [pipenv](https://github.com/pypa/pipenv).

- [loguru](https://github.com/Delgan/loguru/)
- [py_loguru_wrapper](https://gitlab.inria.fr/aljanon/py_loguru_wrapper)
- [py_command_wrapper](https://gitlab.inria.fr/aljanon/py_command_wrapper)

### Guix-managed dependencies

- Python 3
- pip
- pipenv
- git
- util-linux (dmesg)
