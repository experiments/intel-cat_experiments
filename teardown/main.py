#!/usr/bin/python3

# teardown_experiment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from loguru import logger
from pathlib import Path
from py_loguru_wrapper import setup_global_logger, teardown_global_logger
from teardown_experiment.info import write_info
from shutil import make_archive, rmtree

INFO_DIR_NAME="teardown"

@logger.catch
def main():
    results_dir: Path = Path(sys.argv[1]).resolve()
    info_dir: Path = results_dir / INFO_DIR_NAME
    setup_global_logger(info_dir)
    logger.info("Logging setup.")
    write_info(info_dir)
    teardown_global_logger()
    logger.info(f"Archiving {INFO_DIR_NAME} directory...")
    make_archive(results_dir / INFO_DIR_NAME, "xztar", root_dir=info_dir)
    logger.info(f"{INFO_DIR_NAME} archived. Cleaning up...")
    rmtree(info_dir)
    logger.info("Cleanup complete.")
    logger.info("Printing results directory:")

if __name__=="__main__":
    main()
