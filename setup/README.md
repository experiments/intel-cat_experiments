# setup_experiment

This repository contains a simple Python program collecting hardware
and software information about the current system.

It first creates a directory to store all related experiment results, metadata,
and miscellaneous information. This directory is created at the root of the
current git repository with an
[ISO-8601](https://en.wikipedia.org/wiki/ISO_8601)-formatted timestamp. The
absolute path to this directory is output to stdout at the end of the execution.

## Collected information

`setup_experiment` collects information from the following utilities:

- cpuid
- dmidecode
- lsblk
- lscpu
- lshw
- lsmod
- lspci
- hwloc (topology)
- uname
- /proc/cmdline
- /proc/config.gz
- /proc/version

## Usage

`setup_experiment` has one mandatory argument: the path to a writable
directory where it will create the results directory.

If you plan to use the provided Pipfile for managing python dependencies, do not
forget to `lock` the file first.

If you have Guix (with support for Inferiors) installed, simply execute the
`run` script. It will read the list of packages and custom repositories from the
[environment.scm](./environment.scm) file. Otherwise, you can install the
packages listed in [Guix-managed dependencies].

## Dependencies

### Python dependencies

The Python dependencies are managed by [pipenv](https://github.com/pypa/pipenv).

- [loguru](https://github.com/Delgan/loguru/)
- [py_loguru_wrapper](https://gitlab.inria.fr/aljanon/py_loguru_wrapper)
- [py_command_wrapper](https://gitlab.inria.fr/aljanon/py_command_wrapper)

### Guix-managed dependencies

- Python 3
- pip
- pipenv
- git
- cpuid
- dmidecode
- util-linux (lsblk, lscpu)
- lshw
- module-init-tools (lsmod)
- pcituils (lspci)
- hwloc
- coreutils (uname)
