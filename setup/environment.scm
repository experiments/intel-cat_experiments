;; setup_experiment
;; Copyright (C) 2019  JANON Alexis

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (guix inferior)
             (guix channels)
             (srfi srfi-1))

(define channels
  (list
   (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit "ca2b1dcfcd73931358dcd1f9bf79d717044e80a8"))
   (channel
    (name 'custom)
    (url "https://gitlab.inria.fr/aljanon/guix-packages.git")
    (commit "bedddb896f96f9c93fcc12ed52cdd94a00d0fe95"))))

(define inferior
  (inferior-for-channels channels))

(define inferior-specifications->manifest
  (lambda (inferior lst)
    (packages->manifest
     (map (lambda (x)
            (first (apply lookup-inferior-packages inferior x)))
          lst))))

;; TODO: lshw -json and lshw -dump do not seem available in the environment
(inferior-specifications->manifest
 inferior
 '(("python" "3")
   ;; script dependencies
   ("python-loguru")
   ("python-py-command-wrapper")
   ("python-py-loguru-wrapper")
   ;; for retrieving information about the machine
   ("cpuid")
   ("dmidecode")
   ("util-linux") ; lsblk, lscpu
   ("lshw")
   ("module-init-tools") ; lsmod
   ("pciutils") ; lscpi
   ("hwloc" "2")
   ("coreutils") ; uname
   ;; additional setup
   ;; for building cache_access_time
   ("gcc-toolchain")
   ("gsl")
   ))
