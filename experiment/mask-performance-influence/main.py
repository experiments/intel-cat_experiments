#!/usr/bin/python

# intel-cat_experiments
# Copyright (C) 2019 Alexis Janon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import json
import multiprocessing as mp
import random
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import List

import hwloc
from loguru import logger

from py_command_wrapper import RunArgs
from py_loguru_wrapper import (GlobalFileLogger, TTYLoggerOptions,
                               teardown_global_logger)

from .cache_control import IntelCatInfo


# TODO: add enqueue=True by default in setup_global_logger in py_loguru_wrapper
def setup_global_logger(wd: Path) -> None:
    logger.configure(
        handlers=[
            vars(TTYLoggerOptions(enqueue=True)),
            vars(
                GlobalFileLogger(
                    sink=wd / "TRACE.log",
                    level="TRACE",
                    serialize=True,
                    backtrace=True,
                    enqueue=True,
                )
            ),
            vars(GlobalFileLogger(sink=wd / "DEBUG.log", level="DEBUG", enqueue=True)),
            vars(
                GlobalFileLogger(
                    sink=wd / "ERROR.log", level="ERROR", delay=True, enqueue=True
                )
            ),
        ],
        levels=[
            {"name": "DEBUG", "color": "<bold>"},
            {"name": "INFO", "color": "<bold><blue>"},
            {"name": "CRITICAL", "color": "<bold><magenta>"},
        ],
    )
    logger.info(f"Global logs dir: {wd}")


DIR_NAME: str = "mask-performance-influence"

SEED_RANGE: range = range(0, 4)
AVERAGE_ACCESS_COUNT: int = 4
ARRAY_SIZE_COUNT: int = 30


@dataclass
class Configuration:
    seed: int
    core_logical: int
    cache_mask: str
    array_size: int
    access_count: int


def experiment(work_dir: Path, bin_path: Path, package):
    cuttr_transaction = {
        "commands": [
            {
                "command": "spawn",
                "name": "cache_access_time",
                "args": ["cpuset", "resctrl"],
                "properties": {},
                "cmd": str(bin_path),
            }
        ]
    }
    cat_info = IntelCatInfo()
    results = []
    topology = hwloc.Topology()
    topology.load()
    package_os_list = [
        package.os_index for package in topology.objs_by_type(hwloc.OBJ_PACAGE)
    ]
    numa = package.get_ancestor_obj_by_type(hwloc.OBJ_NUMANODE)
    random.seed(0)
    configuration_list: List[Configuration] = []
    # Assuming all processors are equivalent, it is not necessary to run all
    # experiments on all of them. In this case, the seeds can be distributed
    # among all available processors.
    # Thus: only take every nth element (with n being the index of the current
    # processor).
    # NOTE: [0, 1, 2, 3, 4, 5, 6, 7][0::2] == [0, 2, 4, 6]
    # [0, 1, 2, 3, 4, 5, 6, 7][1::2] == [1, 3, 5, 7]
    for seed in SEED_RANGE[package.logical_index :: len(package_os_list)]:
        for core in topology.objs_inside_cpuset_by_type(package.cpuset, hwloc.OBJ_CORE):
            cache = topology.get_shared_cache_covering_obj(core)
            cache_size_max = cache.attr.cache.size
            array_size_list: List[int] = [
                int(cache_size_max / (ARRAY_SIZE_COUNT - 1) * i)
                for i in range(1, ARRAY_SIZE_COUNT - 1)
            ] + [cache_size_max]
            for cache_mask in cat_info.mask_list():
                for array_size in array_size_list:
                    configuration_list.append(
                        Configuration(
                            seed,
                            core.logical_index,
                            cache_mask,
                            array_size,
                            AVERAGE_ACCESS_COUNT,
                        )
                    )
    # Shuffle the configuration list
    random.shuffle(configuration_list)
    for configuration in configuration_list:
        core = topology.get_obj_by_type(hwloc.OBJ_CORE, configuration.core_logical)
        cpu_list = list(topology.objs_inside_cpuset_by_type(core.cpuset, hwloc.OBJ_PU))
        cpu_os = ",".join([str(pu.os_index) for pu in cpu_list])
        cpu_logical = ",".join([str(pu.logical_index) for pu in cpu_list])
        cache = topology.get_shared_cache_covering_obj(core)
        cache_size_max = cache.attr.cache.size
        cache_size = (
            cache_size_max
            * bin(int(configuration.cache_mask, 16)).count("1")
            // bin(cat_info.cbm_mask).count("1")
        )
        # The cache mask must be different than 0
        schemata_dict = {
            p_index: format(cat_info.cbm_mask, "x") for p_index in package_os_list
        }
        schemata_dict[package.os_index] = configuration.cache_mask
        schemata = (
            "L3:" + ";".join([f"{p}={schemata_dict[p]}" for p in schemata_dict]) + "\n"
        )
        logger.info(schemata)
        exp_logger = logger.bind(
            cpu_os=cpu_os,
            cpu_logical=cpu_logical,
            core_os=core.os_index,
            core_logical=core.logical_index,
            package_os=package.os_index,
            package_logical=package.logical_index,
            numa_os=numa.os_index,
            numa_logical=numa.logical_index,
            cache_mask=configuration.cache_mask,
            schemata=schemata,
            cache_depth=cache.attr.cache.depth,
            cache_size=cache_size,
            cache_size_max=cache_size_max,
            seed=configuration.seed,
            array_size=configuration.array_size,
            average_access_count=configuration.access_count,
        )
        exp_logger.info(
            f"Running experiment with seed={configuration.seed}, cache "
            f"size={cache_size} and cache mask={configuration.cache_mask} on "
            f"CPUs {cpu_logical} (os: {cpu_os}) and NUMA node "
            f"{numa.logical_index} (os: {numa.os_index})"
        )
        cuttr_cmd = cuttr_transaction["commands"][0]
        cuttr_cmd["properties"] = {
            "schemata": str(schemata),
            "cpuset.cpus": str(cpu_os),
            "cpuset.mems": str(numa.os_index),
        }
        cuttr_cmd["cmd_args"] = [
            str(configuration.seed),
            str(configuration.array_size),
            str(configuration.access_count),
        ]
        completed = RunArgs(
            work_dir, ["cuttr", "transaction", json.dumps(cuttr_transaction)]
        ).run()
        if completed.returncode != 0:
            exp_logger.error("Experiment failed.")
            continue
        try:
            duration: int = int(completed.stdout)
        except ValueError as e:
            exp_logger.error(f"Experiment failed. Cannot convert return string: {e}")
            continue
        exp_logger.info(f"Experiment lasted: {duration}")
        results.append(
            {
                "cpu_os": cpu_os.replace(",", ";"),
                "cpu_logical": cpu_logical.replace(",", ";"),
                "core_os": str(core.os_index),
                "core_logical": str(core.logical_index),
                "package_os": str(package.os_index),
                "package_logical": str(package.logical_index),
                "numa_os": str(numa.os_index),
                "numa_logical": str(numa.logical_index),
                "cache_mask": str(configuration.cache_mask),
                "schemata": str(schemata),
                "cache_depth": str(cache.attr.cache.depth),
                "cache_size": str(cache_size),
                "cache_size_max": str(cache_size_max),
                "seed": str(configuration.seed),
                "array_size": str(configuration.array_size),
                "average_access_count": str(configuration.access_count),
                "duration": str(duration),
            }
        )
    return results


def write_results(results_file: Path, results) -> None:
    fieldnames = results[0].keys()
    if not results_file.exists():
        logger.info(f"Creating results file {results_file} and writing header.")
        with open(results_file, "w", newline="") as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writeheader()
    logger.info(f"Writing results to {results_file}.")
    with open(results_file, "a", newline="") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        for line in results:
            writer.writerow(line)
    logger.info(f"Wrote {len(results)} records to {results_file}.")


def wrapper(package):
    """Bind the current python process to the specified package, run the experiment
    and return the results.

    """
    topology = hwloc.Topology()
    topology.load()
    topology.set_cpubind(package.cpuset, hwloc.CPUBIND_PROCESS | hwloc.CPUBIND_STRICT)
    base_dir: Path = Path(sys.argv[1]).resolve()
    results_dir: Path = Path(sys.argv[2]).resolve()
    work_dir: Path = results_dir / DIR_NAME
    cache_access_time_bin: Path = base_dir / "experiment/cache_access_time/cache_access_time"
    return experiment(work_dir, cache_access_time_bin, package)


@logger.catch(level="CRITICAL", reraise=True)
def main():
    """Initialize the experiment and use the multiprocess python library to
    dispatch the work between all processors."""
    results_dir: Path = Path(sys.argv[2]).resolve()
    results_file: Path = results_dir / "results.csv"
    work_dir: Path = results_dir / DIR_NAME
    setup_global_logger(work_dir)
    logger.info("Logging setup.")
    topology = hwloc.Topology()
    topology.load()
    package_list = list(topology.objs_by_type(hwloc.OBJ_PACAGE))
    with mp.Pool(processes=len(package_list)) as pool:
        results = [
            item for sublist in pool.map(wrapper, package_list) for item in sublist
        ]
    write_results(results_file, results)
    teardown_global_logger()


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.critical(f"Uncaught exception: {e}. Aborting.")
        sys.exit(1)
    else:
        sys.exit(0)
