#!/usr/bin/python

# intel-cat_experiments
# Copyright (C) 2019 Alexis Janon

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from pathlib import Path
from typing import List


class IntelCatInfo:
    """Singleton containing lazily obtained information about the current processor
    CAT capabilities."""

    class _IntelCatInfo:
        _BASE_PATH: Path = Path("/sys/fs/resctrl/info/L3")

        @property
        def cbm_mask(self) -> int:
            if not hasattr(self, "_cbm_mask"):
                self._cbm_mask = int(
                    IntelCatInfo._IntelCatInfo._BASE_PATH / "cbm_mask".read_text(), 16
                )
            return self._cbm_mask

        @property
        def min_cbm_bits(self) -> int:
            if not hasattr(self, "_min_cbm_bits"):
                self._cbm_mask = int(
                    IntelCatInfo._IntelCatInfo._BASE_PATH / "min_cbm_bits".read_text()
                )
            return self._min_cbm_bits

        @property
        def mask_list(self) -> List[str]:
            if not hasattr(self, "_mask_list"):
                base_mask = (2 ** self.min_cbm_bits) - 1
                self._mask_list = []
                while base_mask <= self.cbm_mask:
                    mask = base_mask
                    while mask <= self.cbm_mask:
                        self._mask_list.append(format(mask, "x"))
                        mask *= 2
                    base_mask = base_mask * 2 + 1
            # return a copy of the list
            return self._mask_list[:]

    instance = None

    def __init__(self):
        if not IntelCatInfo.instance:
            IntelCatInfo.instance = IntelCatInfo._IntelCatInfo()

    def __getattr__(self, name):
        return getattr(self.instance, name)
