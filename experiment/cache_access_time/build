#!/bin/bash

# intel-cat_experiments
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# https://stackoverflow.com/a/246128
# MIT licensed, Copyright (C) 2018 Dave Dopson
declare -r DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

declare -r BIN_NAME="${DIR}/cache_access_time"
declare -r CC="gcc"
declare -r FILES="${DIR}/cache_access_time.c"
declare -r LIBS="-lgsl -lgslcblas -lm"
declare -r LINESIZE="$(getconf LEVEL1_DCACHE_LINESIZE)" \
        || printf "Could not retrieve cache line size. Will use the default value in the source file.\n"
declare -r COMPILE_FLAGS="-Ofast -Wall -Wextra -D LINESIZE=${LINESIZE}"

"$CC" ${COMPILE_FLAGS} ${LIBS} -o "${BIN_NAME}" "${FILES}"
