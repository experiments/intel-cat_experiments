#ifndef LINESIZE
#define LINESIZE 64
#endif

#include <stdio.h> // perror, printf
#include <stdlib.h> // atoi, calloc, exit, EXIT_FAILURE, EXIT_SUCCESS, free
#include <time.h> // clock_gettime, CLOCK_REALTIME, timespec
#include <math.h> // log2, round
#include <gsl/gsl_randist.h> // gsl_rng, gsl_rng_alloc, gsl_rng_set, gsl_rng_free

struct elem {
	int v;
	struct elem *n;
} __attribute__((aligned(LINESIZE)));

int main(int argc, char **argv)
{
  if (argc < 4) {
    printf("Not enough arguments. Aborting.\n");
    exit(EXIT_FAILURE);
  }
  unsigned int seed = atoi(argv[1]);
  unsigned int nmemb = atoi(argv[2]);
  unsigned int access_count = atoi(argv[3]);
  if (nmemb == 0 || access_count == 0) {
    printf("nmemb and access_count must be non-null. Aborting.\n");
    exit(EXIT_FAILURE);
  }
	struct elem *array;
	if(!(array = calloc(nmemb, sizeof(typeof(*array))))) {
    perror("calloc");
    exit(EXIT_FAILURE);
  }
	/* cf. Swann Perarnau's PhD thesis p.23, fig. 2.7 */
  /* list randomization
   * mt19937 is a high quality rng (Mersenne twister)
   */
	gsl_rng *rng = gsl_rng_alloc(gsl_rng_mt19937);
  if (!rng) {
    perror("gsl_rng_alloc");
    free(array);
    exit(EXIT_FAILURE);
  }
	gsl_rng_set(rng, seed);
	size_t i;
	for(i = 0; i < nmemb; i++) {
		array[i].v = i;
	}
	gsl_ran_shuffle(rng, (void*) array, nmemb, sizeof(struct elem));
	struct elem *cur = &(array[array[0].v]);
	for(i= 0; i < nmemb; i++) {
		cur->n = &(array[array[i].v]);
		cur = cur->n;
	}
	cur->n = &(array[array[0].v]);
	gsl_rng_free(rng);
  /* number of accessed elements
   * TODO: is this really the number we need?
   */
	size_t loop_count = round(nmemb * log2(nmemb) + (access_count - 1) * log2(log2(nmemb)));
  /* measure cache access time */
	struct timespec start, end;
	clock_gettime(CLOCK_REALTIME, &start);
  /* main access loop */
	cur = &(array[array[0].v]);
  /* volatile prevents gcc from optimizing the loop */
	volatile int sum = 0;
	for(i = 0; i < loop_count; ++i) {
    sum += cur->v;
		cur = cur->n;
	}
  /* retrieve duration */
	clock_gettime(CLOCK_REALTIME, &end);
	long nsecs = (end.tv_nsec - start.tv_nsec)
    + 1e9 * (end.tv_sec - start.tv_sec);
	printf("%ld\n", nsecs);
	free(array);
	exit(EXIT_SUCCESS);
}
