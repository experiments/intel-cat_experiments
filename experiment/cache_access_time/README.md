# Parasite

This a simple parasite implementation based on
[Swann Perarnau](https://www.mcs.anl.gov/~perarnau/)'s PhD Thesis. The thesis is
available at [https://tel.archives-ouvertes.fr/tel-00650047].

## Usage

Compile the software using the provided build script. This application requires
two values to run: the seed for the array randomization, the size of the array
(number of elements the length of the cache line size), and the average access
count per element, in this order.

```sh
./cache-access-time <seed> <array_size> <average_access_count>
```

## Implementation notes

For this implementation to work properly, all caches should have the same cache
line size. The line size is passed as a constant retrieved from `getconf`:
```sh
getconf LEVEL1_DCACHE_LINESIZE
```

The implementation was made as minimal as possible in order to reduce the
initialization overhead and interference.
