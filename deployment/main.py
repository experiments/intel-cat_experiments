# deployment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import sys
from datetime import datetime, timedelta
from pathlib import Path

from git import Repo

from enoslib.api import generate_inventory, run_ansible
from enoslib.errors import EnosFailedHostsError, EnosUnreachableHostsError
from enoslib.infra.configuration import BaseConfiguration
from enoslib.infra.enos_g5k.configuration import Configuration
from enoslib.infra.enos_g5k.g5k_api_utils import get_api_client
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.provider import Provider

TIMEOUT: timedelta = timedelta(hours=2)
RUN_TIMEOUT: timedelta = timedelta(hours=1, minutes=30)

RESET: str = "\033[0m"
GREEN: str = "\033[32m"

logging.basicConfig(
    format=f"{GREEN}[%(asctime)s]{RESET} %(levelname)-8s: %(message)s",
    level=logging.INFO,
)


def main():
    repo: Repo = Repo(search_parent_directories=True)
    if repo.is_dirty():
        logging.critical(
            "The repository is dirty. Please stash or commit changes before proceeding. Aborting deployment."
        )
        return 3
    if RUN_TIMEOUT > TIMEOUT:
        logging.critical(
            f"RUN_TIMEOUT (={RUN_TIMEOUT}) is longer than TIMEOUT (={TIMEOUT}). Aborting deployment."
        )
        return 3
    repo_dir: Path = Path(repo.working_tree_dir).absolute()

    provider_conf = {
        "job_name": repo_dir.name,
        "env_name": "https://api.grid5000.fr/sid/sites/grenoble/public/ajanon/debian10-min-guix_3606e04d87c932f5c86317f5f9a4860357ab6bc9.dsc",
        "queue": "default",
        "walltime": str(TIMEOUT),
        "dhcp": False,
        "resources": {
            "machines": [
                {
                    "roles": ["machine"],
                    "cluster": "dahu",
                    "nodes": 1,
                    "primary_network": "n1",
                }
            ],
            "networks": [
                {"id": "n1", "type": "kavlan", "roles": ["network"], "site": "grenoble"}
            ],
        },
    }

    ansible_dir: Path = repo_dir / "ansible"
    inventory_path: str = str(ansible_dir / "hosts")
    ansible_path: str = str(ansible_dir / "main.yml")
    extra_vars = {
        "git_root": str(repo_dir),
        "git_commit": repo.head.commit.hexsha,
        "g5k_username": get_api_client().username,
        "timestamp": datetime.utcnow().isoformat(sep="T", timespec="milliseconds")
        + "Z",
        "run_timeout": str(int(RUN_TIMEOUT.total_seconds())),
    }
    conf: BaseConfiguration = Configuration.from_dictionnary(provider_conf)
    try:
        provider: Provider = G5k(conf)
        roles, networks = provider.init()
        generate_inventory(roles, networks, inventory_path, check_networks=False)
        run_ansible(
            [ansible_path],
            inventory_path=inventory_path,
            roles=roles,
            extra_vars=extra_vars,
        )
    except EnosFailedHostsError as e:
        logging.critical(
            f"Hosts failed: {e}. Keeping reservation for debugging. Aborting."
        )
        return 1
    except EnosUnreachableHostsError as e:
        logging.critical(
            f"Hosts unreachable: {e}. Destroying reservation and aborting."
        )
        provider.destroy()
        return 2
    else:
        logging.info(f"Run completed successfully.")
        provider.destroy()
        return 0


if __name__ == "__main__":
    try:
        return_code = main()
        sys.exit(return_code)
    except Exception as e:
        logging.critical(f"Uncaught exception: {e}. Aborting.")
        sys.exit(4)
